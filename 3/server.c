#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(void) {
	static char buf[1024];
	int fd = open("fifo", O_RDONLY);
	while (1) {
		int len = read(fd, buf, 1024);

		if (len == -1) {
			fprintf(stderr, "Error! Can't read from fifo\n");
			exit(__LINE__);
		}

		if (len == 0) { //received EOF
			puts("Seems like pipe is closed. I'm turning off");
			break;
		}

		buf[len] = '\0'; //Set '\0' in end of string
		int i;
		printf("Received:\n");
		for (i = 0; i < 10; i++)
			printf("%slen: %d\n", buf, len);
		sleep(3);
	}
	close(fd);
	return 0;
}

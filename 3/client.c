#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

int main(void) {
	const char *fifo = "./fifo";
	mkfifo(fifo, 0666);
	sigset_t set;
	sigemptyset(&set);
	sigaddset(&set, SIGUSR1);
	sigaddset(&set, SIGUSR2);
	sigprocmask(SIG_BLOCK, &set, NULL); //block default handler
	printf("Pid: %d\n", getpid());

	int fd = open(fifo, O_WRONLY);

	static char buf[1024];
	while (1) {
		int signal;
		if (sigwait(&set, &signal) != 0) {
			printf("Error while wait for signal\n");
			exit(__LINE__);
		}
		if (signal == SIGUSR1) {
			printf("Received SIGUSR1! You should enter a line:\n");
			fgets(buf, 1024, stdin);
			write(fd, buf, strlen(buf));
		}
		if (signal == SIGUSR2) {
			printf("I'm closing. Good bye!\n");
			break;
		}
	}
	close(fd);
	unlink(fifo);
	return 0;
}

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define SIZE	200000000

int main(void) {
	clock_t all_begin = clock();
	srand(time(NULL));
	
	static char buf[SIZE];
	int i;
	for (i = 0; i < SIZE; i++) {
		char c = 'a' + rand() % ('z' - 'a' + 1);
		buf[i] = c;
	}
	
	int fd = open("data2.txt", O_RDWR | O_CREAT, 0600);
	if (fd < 0) {
		write(2, "Error! Can't open file!\n", strlen("Error! Can't open file!\n") + 1);
		exit(__LINE__);
	}

	clock_t begin = clock();
	write(fd, buf, SIZE - 1);
	close(fd);
	clock_t end = clock();
	printf("Time all:   %lf\nTime print: %lf\n", (double)(end - all_begin), (double)(end - begin));
	return EXIT_SUCCESS;
}

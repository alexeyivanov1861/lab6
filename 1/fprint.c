#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE	200000000

int main(void) {
	clock_t all_begin = clock();
	FILE* file = fopen("data1.txt", "w");
	if (!file) {
		fprintf(stderr, "Error! Can't open file!\n");
		exit(__LINE__);
	}

	srand(time(NULL));

	static char buf[SIZE];
	
	int i;
	for (i = 0; i < SIZE - 1; i++) {
		char c = 'a' + rand() % ('z' - 'a' + 1);
		buf[i] = c;
	}

	clock_t begin = clock();

	for (i = 0; i < SIZE - 1; i++) {
		fputc(buf[i], file);
	}
	fclose(file);

	clock_t end = clock();
	printf("Time all:   %lf\nTime print: %lf\n", (double)(end - all_begin), (double)(end - begin));
	return EXIT_SUCCESS;
}

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

char** getdata(int *count) {
	FILE *in = fopen("input.txt", "r");
	if (!in)
		exit(__LINE__);
	int n = 0;
	int capacity = 100;
	char **str = malloc(sizeof(char*) * capacity);
	char *buf = malloc(sizeof(char) * 100);
	while (fgets(buf, 100, in) != NULL) {
		int len = strlen(buf);
		buf[len - 1] = '\0';
		if (n >= capacity) {
			str = realloc(str, 2 * capacity * sizeof(char *));
		}
		str[n] = malloc(sizeof(char) * len);
		memcpy(str[n], buf, sizeof(char) * len);
		n++;
	}
	str = realloc(str, n* sizeof(char*));
	*count = n;
	free(buf);
	fclose(in);
	return str;
}

void clear(char **str, int n) {
	int i;
	for (i = 0; i < n; i++)
		free(str[i]);
	free(str);
}

void print(char** str, int n) {
	FILE *out = fopen("output.txt", "w");
	if (!out)
		exit(__LINE__);
	int i;
	for (i = 0; i < n; i++) {
		fprintf(out, "%s\n", str[i]);
	}
	fclose(out);
}

int cmp(const void *p1, const void *p2) {
	return strcmp(*(char **)p1, *(char **)p2);
}

void child() {
	int n;
	char **str = getdata(&n);
	qsort(str, n, sizeof(char*), cmp);
	print(str, n);
	clear(str, n);
}

int main(void) {
	int pid = fork();
	int status;
	if (!pid) {
		child();
		return EXIT_SUCCESS;
	}
	int i = 1;
	while (1) {
		printf("Son entered %d class\n", i);
		sleep(3);
		printf("Dad told him: \"If you will study well, then i will buy you whatever you want\n");
		sleep(3);
		printf("Son finished %d class with good marks and tells his father\n", i);
		printf("Son: Father, buy me desk\n");
		sleep(3);
		printf("Father: OK, I'wll buy desk for you, but why you asking for it?\n");
		sleep(3);
		printf("Son: I just want desk\n");
		sleep(3);
		++i;
		if (waitpid(pid, &status, WNOHANG))
			break;
	}
	printf("Son died and no one knows for what he wanted desks\n");
	printf("Process ended with status %d\n", status);
	return 0;
}


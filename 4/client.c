#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>

#define BUFSIZE	8192
#define MY_PORT 31234

void get_list_of_ip(const char* dns, char *buf) {
	*buf = '\0';
	struct hostent *h;
	h = gethostbyname(dns);

	if (!h) {
		fprintf(stderr, "Error while gethostbyname\n");
		exit(EXIT_FAILURE);
	}

	strcat(buf, "DNS: ");
	strcat(buf, dns);
	strcat(buf, "\n");

	int i = 0;
	while (h->h_addr_list[i] != NULL) {
		struct in_addr* tmp = (struct in_addr*) h->h_addr_list[i];
		strcat(buf, inet_ntoa(*tmp));
		strcat(buf, "\n");
		i++;
	}
	printf("%s", buf);
}

int main(int argc, char *argv[]) {
	if (argc < 3) {
		fprintf(stderr, "Usage: %s <DNS_NAME> <IP_SERVER_TO_CONNECT>\n", argv[0]);
		return EXIT_SUCCESS;
	}

	static char buf[BUFSIZE];
	get_list_of_ip(argv[1], buf);

	//make socket
	int s = socket(AF_INET, SOCK_STREAM, 0);
	if (!s) {
		fputs("Error while creating socket", stderr);
		return EXIT_FAILURE;
	}
	
	const char* server_ip = argv[2]; //server ip
	struct sockaddr_in si; //server info
	memset(&si, 0, sizeof(si));
	si.sin_family = AF_INET;
	si.sin_port = (in_port_t)htons(MY_PORT);
	inet_aton(server_ip, &si.sin_addr); //in server there another fuction

	if (connect(s, (struct sockaddr*) &si, sizeof(si)) == -1) {
		fprintf(stderr, "Can't connect to server\n");
		return EXIT_FAILURE;
	}

	if (send(s, buf, strlen(buf), 0) == -1) {
		fprintf(stderr, "Can't send info to server\n");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}


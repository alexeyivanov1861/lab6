#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <signal.h>

#define MY_PORT 31234

FILE* in;

void my_handler(int sing) {
	printf("\nCaught SIGINT. Good bye.\n");
	fclose(in);
	exit(0);
}

int main(void) {
	if (signal(SIGINT, my_handler) == SIG_ERR) {
		fprintf(stderr, "Can't set handler\n");
		return -1;
	}
	//make socket
	int s = socket(PF_INET, SOCK_STREAM, 0);
	if (s == -1) {
		fprintf(stderr, "Can't create socket\n");
		return -1;
	}
	//fill some info
	struct sockaddr_in name;
	name.sin_family = AF_INET;
	name.sin_port = (in_port_t)htons(MY_PORT);
	name.sin_addr.s_addr = htonl(INADDR_ANY);

	//bind socket
	if (bind(s, (struct sockaddr *) &name, sizeof(name)) == -1) {
		fprintf(stderr, "Can't bind\n");
		return -1;
	}
	
	//define lenght if queue
	if (listen(s, 10) == -1) {
		fprintf(stderr, "Cant set len of queue\n");
		return -1;
	}

	static char buf[8192];

	in = fopen("dns_ip.txt", "a+");

	while (1) {
		int cs = accept(s, NULL, NULL);
		if (cs == -1) {
			fprintf(stderr, "Cant accept\n");
			return -1;
		}

		read(cs, buf, 8192);
		printf("Received: %s", buf);
		fprintf(in, "%s", buf);
		sleep(1);
	}

	return 0;
}
